﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPF_MVVM.Models;
using WPF_MVVM.ViewModels;

namespace WPF_MVVM
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
       public ItemsVM vm { get { return DataContext as ItemsVM; }}
        public MainWindow()
        {
            InitializeComponent();
        }

        public void AddItem(object sender, RoutedEventArgs e)
        {
            AddWindow addWindow = new AddWindow();
            addWindow.Show();
        }

        private void AddOneParticipantButton_Click(object sender, RoutedEventArgs e)
        {
            vm.AddParticipantToTodo((Items)(ItemsListView.SelectedItem));
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            vm.DeleteTodo((Items)(ItemsListView.SelectedItem));
        }



        private async void SaveToFile_Click(object sender, RoutedEventArgs e)
        {
            await vm.SaveToFileJson();
        }

        private async void LoadFromFile_Click(object sender, RoutedEventArgs e)
        {
            await vm.LoadFromFileJson();

        }
    }
}
